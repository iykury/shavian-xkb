# Shavian XKB Layout Collection by Iykury

This is a collection of Shavian keyboard layouts for use in Linux.

## Layouts

Currently, the collection includes three layouts.

### Iykury

This is my own layout, inspired by Dvorak. Vowels are on the left and consonants are on the right (mostly). The [26 most common letters](https://www.reddit.com/r/shavian/comments/ovke9g/shavian_letter_frequencies/) are arranged with the most common on the home row and generally closer to the center. The remaining 22 are typed by holding shift and pressing the key for another letter, and I've tried to make these easy to remember if possible (though it's definitely not perfect). The punctuation keys are all in the same positions as regular US QWERTY except for the semicolon/colon key, which has been moved to the bottom-left, like in Dvorak.

It also includes a key for Variation Selector 1 (or "VS1"), which is a character that can be used with the font [Inter Alia](https://github.com/Shavian-info/interalia) to type some extra Shavian characters not included in Unicode.

![Iykury layout map](images/iykury.png)

### Imperial

This is mostly identical to the "Shaw Imperial" layout from [Shavian.info](https://www.shavian.info/keyboards/), though I've modified it so that shift+𐑠 (which is where shift+\\ is located on QWERTY) types the VS1 character mentioned earlier.

![Imperial layout map](images/igc.png)

### QWERTY

This is mostly identical to the "Shaw QWERTY" layout from [Shavian.info](https://www.shavian.info/keyboards/), though, like Imperial, I've modified it so that shift+\\ types the VS1 character.

![QWERTY layout map](images/qwerty.png)

## Installation

As far as I know, XKB doesn't have a good way to install custom layouts, so you'll need root permissions to modify the configuration files. I once made a script to do this automatically, but I figured it was too dangerous, so I've written instructions on how to do it manually.

**WARNING: These instructions have worked for me, but I cannot guarantee that they will work for you, and in the worst case they could make your keyboard unusable. Proceed with caution.**

1. Locate where XKB stores its config files. For me, it's `/usr/share/X11/xkb/`, though it may differ between distros. I'll refer to this directory as simply `[xkb]`.

2. Copy the file `shavian` to `[xkb]/symbols/`

3. Open `[xkb]/rules/evdev.xml`, find the line that says `</layoutList>` (make sure the slash is there), and insert the contents of `add_to_evdev.xml` directly before that line. I'd recommend making a backup of `[xkb]/rules/evdev.xml` first because if you do this incorrectly, your keyboard will become unusable, and it'll be much more difficult to fix without a backup.

4. Relog or restart your computer. (This step may not be necessary.)

5. You should now be able to select the new Shavian layouts in your settings program.

NOTE: Sometimes an update will overwrite `[xkb]/rules/evdev.xml`, so if the layouts stop working at some point, you may need to repeat steps 3 and 4.

If you've followed these instructions correctly and they still don't work, [contact me](https://iykury.xyz/contact/) so I can update them.
